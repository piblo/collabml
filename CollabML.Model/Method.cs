﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CollabML.Data.Model
{
    public class Method
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int ClassId { get; set; }
        [IgnoreDataMember]
        public Class Class { get; set; }
    }
}
