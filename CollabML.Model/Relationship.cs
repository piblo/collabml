﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace CollabML.Data.Model
{
    public class Relationship
    {
        public int Id { get; set; }

        [ForeignKey("SourceClass")]
        public int SourceClassId { get; set; }
        [IgnoreDataMember]
        public Class SourceClass { get; set; }
        
        [ForeignKey("TargetClass")]        
        public int TargetClassId { get; set; }
        [IgnoreDataMember]
        public Class TargetClass { get; set; }

        public string CardinalitySrc { get; set; }
        public string CardinalityTgt { get; set; }        

        public int BoardId { get; set; }

        public int RelationshipTypeId { get; set; }
        public RelationshipType RelationshipType { get; set; }
    }
}
