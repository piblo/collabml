﻿namespace CollabML.Data.Model
{
    public class ClassType
    {
        public ClassType(){}
        public ClassType(string name)
        {
            Name = name;
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }
}
