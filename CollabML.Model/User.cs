﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace CollabML.Data.Model
{
    public class User
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }

        [Required]
        [EmailAddress]
        public string UserName { get; set; }

        public string Name { get; set; }
        public string Surname { get; set; }
       
        [NotMapped]
        public string Password { get; set; }

        [IgnoreDataMember]
        public List<Board> Boards { get; set; }
    }
}
