﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CollabML.Data.Model
{
    public class Class
    {
        public int Id { get; set; }
        public string Name { get; set; }        

        public int ClassTypeId { get; set; }
        public ClassType ClassType { get; set; }
        public int PositionX { get; set; }
        public int PositionY { get; set; }

        public virtual List<Attribute> Attributes { get; set; }
        public virtual List<Method> Methods { get; set; }        

        public int BoardId { get; set; }
    }
}
