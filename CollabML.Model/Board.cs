﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CollabML.Data.Model
{
    public class Board
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual List<Class> Classes { get; set; }
        public virtual List<Relationship> Relationships { get; set; }

        [IgnoreDataMember]
        public List<User> Users { get; set; }
    }
}
