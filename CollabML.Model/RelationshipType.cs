﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollabML.Data.Model
{
    public class RelationshipType
    {
        public RelationshipType(){}

        public RelationshipType(string type)
        {
            Type = type;
        }

        public int Id { get; set; }
        public string Type { get; set; }
    }
}
