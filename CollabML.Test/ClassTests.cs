﻿using System;
using CollabML.Web.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CollabML.Test
{
    [TestClass]
    public class ClassTests
    {
        [TestMethod]
        public void ClassType_IsGreaterThanZero()
        {
            //arrange
            var cls = new ClassModel { ClassTypeId = 0, BoardId = 1 };
            var validator = new ModelValidator();
            
            //act
            var isValid = validator.ValidateModel(cls);

            //assert
            Assert.AreEqual(false, isValid);
        }

        [TestMethod]
        public void ClassType_IsLowerThanFour()
        {
            //arrange
            var cls = new ClassModel { ClassTypeId = 4, BoardId = 1 };
            var validator = new ModelValidator();

            //act
            var isValid = validator.ValidateModel(cls);

            //assert
            Assert.AreEqual(false, isValid);
        }

        [TestMethod]
        public void ClassType_IsInRange()
        {
            //arrange
            var cls = new ClassModel { ClassTypeId = 2, BoardId = 1 };
            var validator = new ModelValidator();

            //act
            var isValid = validator.ValidateModel(cls);

            //assert
            Assert.AreEqual(true, isValid);
        }

        [TestMethod]
        public void BoardId_GreaterThanZero()
        {
            //arrange
            var cls = new ClassModel { ClassTypeId = 2, BoardId = 0 };
            var validator = new ModelValidator();

            //act
            var isValid = validator.ValidateModel(cls);

            //assert
            Assert.AreEqual(false, isValid);
        }
    }
}
