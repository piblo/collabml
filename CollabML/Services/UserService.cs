﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CollabML.Data.Contracts;
using CollabML.Data.Model;

namespace CollabML.Web.Services
{
    public class UserService
    {
        private IUserRepository userRepository;
        private IIdentityRepository identityRepository;
        public UserService(IUserRepository userRepository, IIdentityRepository identityRepository)
        {
            this.userRepository = userRepository;
            this.identityRepository = identityRepository;
        }

        public User GetUser(string username)
        {
            var identityUser = identityRepository.GetUser(username);
            var user = userRepository.GetUser(identityUser.UserId);
            return user;
        }
    }
}