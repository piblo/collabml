﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using CollabML.Data.Contracts;
using CollabML.Identity;
using Microsoft.Owin.Security.OAuth;

namespace CollabML.Web.Providers
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            //context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            try
            {
                //TODO: Use the repo
                using (IIdentityRepository repo = new IdentityRepository())
                {
                    var user = repo.GetUser(context.UserName, context.Password);

                    if (user == null)
                    {
                        context.SetError("invalid_grant", "Nombre de usuario o contraseña incorrecto.");
                        return;
                    }
                }
                
                var identity = new ClaimsIdentity(new GenericIdentity(
                    context.UserName, OAuthDefaults.AuthenticationType),
                    context.Scope.Select(x => new Claim("urn:oauth:scope", x))
                    );                                

                context.Validated(identity);
            }catch(Exception e)
            {
                context.SetError("invalid_grant", e.Message);                
            }
        }
    }
}