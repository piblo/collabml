﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CollabML.Web.Models;
using Microsoft.AspNet.SignalR;

namespace CollabML.Web.Hubs
{
    public class BaseHub : Hub
    {
        protected ModelValidator ModelValidator = new ModelValidator();
    }
}