﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CollabML.Data.Contracts;
using CollabML.Data.Model;
using CollabML.Web.Models;
using Microsoft.AspNet.SignalR;

namespace CollabML.Web.Hubs
{
    public class BoardHub : BaseHub
    {
        private IClassRepository classRepository;
        private IRelationshipRepository relationshipRepository;
        private IAttributeRepository attributeRepository;
        private IMethodRepository methodRepository;

        public BoardHub(IClassRepository classRepository, IRelationshipRepository relationshipRepository, 
            IAttributeRepository attributeRepository, IMethodRepository methodRepository)
        {
            this.classRepository = classRepository;
            this.relationshipRepository = relationshipRepository;
            this.attributeRepository = attributeRepository;
            this.methodRepository = methodRepository;
        }
        
        [Authorize]
        public void SaveClass(ClassModel model)
        {
            var cls = new Class
                          {
                              Name = model.Name,
                              ClassTypeId = model.ClassTypeId,
                              Attributes = model.Attributes,
                              Id = model.Id,
                              BoardId = model.BoardId,
                              Methods = model.Methods,
                              PositionX = model.PositionX,
                              PositionY = model.PositionY
                          };
            if (ModelValidator.ValidateModel(model))
            {
                try
                {
                    if (cls.Id == 0)
                        cls = classRepository.AddClass(cls);

                    else
                    {
                        foreach (var attribute in cls.Attributes)
                        {
                            if (attribute.Id == 0)
                                attributeRepository.AddAttribute(attribute);
                            else
                                attributeRepository.SaveChanges(attribute);
                        }

                        foreach (var method in cls.Methods)
                        {
                            if (method.Id == 0)
                                methodRepository.AddMethod(method);
                            else
                                methodRepository.SaveChanges(method);
                        }

                        classRepository.SaveChanges(cls);
                        cls = classRepository.GetClass(cls.Id);
                    }
                    Clients.Caller.updateActiveClass(cls);
                }
                catch (Exception e)
                {
                    throw new HubException(e.Message);
                }
                Clients.Others.updateClass(cls);
            }else
                throw new HubException("Error", ModelValidator.ValidationResults);
            
        }

        [Authorize]
        public void DeleteClass(ClassModel model)
        {
            var cls = classRepository.DeleteClass(model.Id);
            Clients.All.deleteClass(cls);
        }

        [Authorize]
        public void SaveLink(Relationship rel)
        {
            Relationship updatedRel;
            try
            {
                if (rel.Id == 0)
                    updatedRel = relationshipRepository.AddRelationship(rel);
                else
                {
                    relationshipRepository.SaveChanges(rel);
                    updatedRel = relationshipRepository.GetRelationship(rel.Id);
                }
                Clients.Caller.updateActiveLink(updatedRel);
            }catch(Exception e)
            {
                throw new HubException(e.Message);
            }
            Clients.Others.updateLink(updatedRel);
        }

        [Authorize]
        public void DeleteLink(Relationship rel)
        {
            rel = relationshipRepository.DeleteRelationship(rel);
            Clients.Others.deleteLink(rel);
        }

        [Authorize]
        public void ChangePosition(Class c)
        {
            try
            {
                Clients.Others.moveElement(c);
                classRepository.UpdatePosition(c);
            }catch(Exception e)
            {
                throw new HubException(e.Message);
            }
        }
    }
}