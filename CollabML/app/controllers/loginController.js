﻿app.controller('loginController', ['$scope', '$location', 'authService', function ($scope, $location, authService) {

    $scope.loginData = {
        userName: "",
        password: ""
    };

    $scope.message = "";
    $scope.alert = "";
    $scope.loading = false;

    $scope.login = function () {
        $scope.loading = true;
        $scope.alert = "";
        authService.login($scope.loginData).then(function (response) {
            $location.path('/board');
        },
         function (err) {
             $scope.message = err.error_description;
             $scope.alert = "alert-shake";
             $scope.loading = false;
         });
    };

}]);