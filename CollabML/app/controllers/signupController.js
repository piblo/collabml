﻿app.controller('signupController', ['$scope', 'authService', function ($scope, authService) {

    $scope.message = "";
    $scope.success = false;
    $scope.errors = [];
    $scope.alert = "";
    $scope.signupData = {
        name: "",
        surname: "",
        userName: "",
        password: ""
    };

    $scope.signup = function () {
        authService.saveRegistration($scope.signupData)
            .then(function (response) {
                $scope.success = true;
                $scope.errors = [];
                $scope.message = "Su cuenta ha sido creada. Haga click en Iniciar Sesión para continuar";
            },
            function (response) {
                $scope.errors = [];
                for (var key in response.data.modelState) {
                    for (var i = 0; i < response.data.modelState[key].length; i++) {
                        $scope.errors.push(response.data.modelState[key][i]);
                    }
                }
                $scope.success = false;
                $scope.alert = "alert-shake";
            });
        $scope.alert = "";
    };

}]);