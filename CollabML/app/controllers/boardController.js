﻿app.controller("boardController", ["$scope", "authService", "$location", "boardFactory", "signalRService", "APIURL",
function ($scope, authService, $location, boardFactory, signalRService, serverUrl) {

    $scope.error = false;
    $scope.errorMessage = "";
    $scope.loading = true;

    var board = {};
    $scope.boardItems = [];
    $scope.boardLinks = [];
    boardFactory.get().then(function(data) {
        board = data;
        $scope.boardItems = data.Classes;
        for (var i = 0; i < $scope.boardItems.length; i++) {
            var item = $scope.boardItems[i];            
            item.jointId = updateCell(item);
        }
        $scope.boardLinks = data.Relationships;
        for (var i = 0; i < $scope.boardLinks.length; i++) {
            var link = $scope.boardLinks[i];
            link.jointId = updateLink(link);
        }
        $scope.loading = false;
    }, function(response) {
        $scope.error = true;
        $scope.errorMessage = response;        
    });

    /* SignalR proxy setup */
    var signalRProxy = signalRService(serverUrl + "signalr/hubs", "BoardHub");

    signalRProxy.on("updateActiveClass", function (cls) {        
        var target = $scope.boardItems.filter(function (i) {
            return i.Id == cls.Id;
        })[0];
        var prev = target;
        target = cls;
        if (prev) {            
            target.jointId = prev.jointId;
        } else {            
            target.jointId = $scope.selectedItem.jointId;
            $scope.boardItems.push(target);
        }
        $scope.selectedItem = target;
        updateCell(cls, target.jointId);
    });

    signalRProxy.on("updateClass", function (cls) {        
        var target = $scope.boardItems.filter(function (i) {
            return i.Id == cls.Id;
        })[0];
        var prev = target;
        var index = $scope.boardItems.indexOf(prev);
        target = cls;
        if(index > 0)
            $scope.boardItems[index] = target;
        if (prev) {
            target.jointId = prev.jointId;           
        } else {           
            $scope.boardItems.push(target);            
        }        
        target.jointId = updateCell(cls, target.jointId);    
    });

    signalRProxy.on("deleteClass", function(cls) {
        var target = $scope.boardItems.filter(function(i) {
            return i.Id == cls.Id;
        })[0];
        if(target) {
            var idx = $scope.boardItems.indexOf(target);
            var cell = $scope.boardGraph.getCell(target.jointId);
            cell.remove();
            $scope.boardItems.splice(idx, 1);            
        }
    });

    signalRProxy.on("updateActiveLink", function(link) {
        var target = $scope.boardLinks.filter(function(i) {
            return i.Id == link.Id;
        })[0];
        var prev = target;
        target = link;
        if(prev) {
            target.jointId = prev.jointId;
        } else {
            target.jointId = $scope.selectedLink.jointId;
            $scope.boardLinks.push(target);
        }
        updateLink(link, target.jointId);
    });

    signalRProxy.on("updateLink", function(link) {
        var target = $scope.boardLinks.filter(function (i) {
            return i.Id == link.Id;
        })[0];
        var prev = target;
        target = link;
        if (prev) {
            target.jointId = prev.jointId;
        } else {
            $scope.boardLinks.push(target);
        }
        target.jointId = updateLink(link, target.jointId);
    });

    signalRProxy.on("deleteLink", function(lnk) {
        var target = $scope.boardLinks.filter(function (i) {
            return i.Id == lnk.Id;
        })[0];
        if (target) {
            var idx = $scope.boardLinks.indexOf(target);
            var cell = $scope.boardGraph.getCell(target.jointId);
            cell.prop("dbId", 0);
            cell.remove();
            $scope.boardLinks.splice(idx, 1);
        }
    });

    signalRProxy.on("moveElement", function(cls) {
        var target = $scope.boardItems.filter(function (i) {
            return i.Id == cls.Id;
        })[0];
        if (target) {
            target.PositionX = cls.PositionX;
            target.PositionY = cls.PositionY;            
            var cell = $scope.boardGraph.getCell(target.jointId);
            var events = cell._events["change:position"][0];
            cell._events["change:position"].splice(0, 1);
            cell.prop("position/x", cls.PositionX);
            cell.prop("position/y", cls.PositionY);            
            cell._events["change:position"].splice(0, 0, events);
        }
    });

    signalRProxy.connect();

    function updateCell(cls, jointId) {
        var cell = $scope.boardGraph.getCell(jointId);
        var options = { size: { width: 150, height: 100 }, position: { x: cls.PositionX, y: cls.PositionY } };
        if(!cell) {
            switch(cls.ClassTypeId) {
                case 1:
                    cell = new uml.Class(options);
                    break;
                case 2:
                    cell = new uml.Abstract(options);
                    break;
                case 3:
                    cell = new uml.Interface(options);
            }
            cell.on('change:position', function () {
                var position = this.position();
                cls.PositionX = position.x;
                cls.PositionY = position.y;
                signalRProxy.invoke("ChangePosition", cls, function(errorMessage, modelErrors) {
                    $scope.error = true;
                    $scope.errorMessage = errorMessage;
                    $scope.modelErrors = modelErrors;
                });
            });
        }
        cell.prop("dbId", cls.Id);
        cell.prop("boarId", cls.BoardId);
        cell.prop("name", cls.Name);
        if (cls.Attributes) {
            var attributes = [];
            for (var i = 0; i < cls.Attributes.length; i++) {
                attributes.push(cls.Attributes[i].Name);
            }
            cell.prop("attributes", attributes);
        }
        if (cls.Methods) {
            var methods = [];
            for (i = 0; i < cls.Methods.length; i++) {
                methods.push(cls.Methods[i].Name);
            }
            cell.prop("methods", methods);
        }        
        
        //Resize
        var itemsCount = cell.attributes.attributes.length + cell.attributes.methods.length;
        var height = 20 * itemsCount;

        var width = cell.attributes.size.width;
        var letterCount = cell.attributes.name.length;
        var count = 0;
        for (i = 0; i < cell.attributes.attributes.length; i++) {
            count = cell.attributes.attributes[i].length;
            if (count > letterCount)
                letterCount = count;
        }
        
        for (i = 0; i < cell.attributes.methods.length; i++) {
            count = cell.attributes.methods[i].length;
            if (count > letterCount)
                letterCount = count;
        }        
        width = letterCount * 6; //~6px / letter

        if (height < 100)
            height = 100;
                
        if (width < 150)
            width = 150;
                
        cell.resize(width, height);

        if (!jointId) {            
            $scope.boardGraph.addCell(cell);            
        }
        
        if(jointId) {
            var cellLinks = $scope.boardGraph.getConnectedLinks(cell);
            if (cellLinks.length) {
                for (var i = 0; i < cellLinks.length; i++) {
                    saveLink(cellLinks[i]);
                }
            }
        }

        return cell.id;
    }    

    function updateLink(lnk, jointId) {
        var link = $scope.boardGraph.getCell(jointId);
        if(!link) {
            switch (lnk.RelationshipTypeId) {
                case 2:
                    link = new uml.Generalization();
                    break;
                case 3:
                    link = new uml.Implementation();
                    break;
                case 4:
                    link = new uml.Aggregation();
                    break;
                case 5:
                    link = new uml.Composition();
                    break;
            }
        }
        var src = $scope.boardItems.filter(function(i) {
            return i.Id == lnk.SourceClassId;
        })[0];
        var tgt = $scope.boardItems.filter(function (i) {
            return i.Id == lnk.TargetClassId;
        })[0];
        link.prop("dbId", lnk.Id);
        link.set("source", { id: src.jointId });
        link.set("target", { id: tgt.jointId });
        if (!jointId) {
            link.on('change:source', function () { saveLink(this); });
            link.on('change:target', function () { saveLink(this); });
            $scope.boardGraph.addCell(link);
        }
        return link.id;
    }

    var uml = joint.shapes.uml;
    var elements = [
        new uml.Class({
            position: { x: 0, y: 0 },
            size: { width: 150, height: 100 },
            name: 'Clase',
            attributes: [],
            methods: []            
        }),
        new uml.Abstract({
            position: { y: 130 },
            size: { width: 150, height: 100 },
            name: 'Clase Abstracta',
            attributes: [],
            methods: []
        }),
        new uml.Interface({
            position: { y: 260 },
            size: { width: 150, height: 100 },
            name: 'Interfaz',
            attributes: [],
            methods: []
        })
    ];

    var links = [
        new uml.Generalization({
            source: { x: 0, y: 10 },
            target: { x: 150, y: 10 }
        }),
        new uml.Implementation({
            source: { x: 0, y: 40 },
            target: { x: 150, y: 40 },
        }),
        new uml.Aggregation({
            source: { x: 0, y: 70 },
            target: { x: 150, y: 70 },
        }),
        new uml.Composition({
            source: { x: 0, y: 100 },
            target: { x: 150, y: 100 },
        })
    ];  
    

    $scope.elementsGraph = new joint.dia.Graph;
    _.each(elements, function (r) { $scope.elementsGraph.addCell(r); });

    $scope.linksGraph = new joint.dia.Graph;
    _.each(links, function (r) { $scope.linksGraph.addCell(r); });        

    $scope.boardGraph = new joint.dia.Graph;    

    $scope.$on("cellPointerClick", function (data, cell, graph) {
        if (graph.cid != $scope.boardGraph.cid) {
            var clon = cell.clone();
            if (cell.isLink()) {
                clon.on('change:source', function () { saveLink(this); });
                clon.on('change:target', function () { saveLink(this); });
            }
            else {
                clon.on('change:position', function () {
                    var self = this;
                    if (this.attributes.dbId) {
                        var cll = $scope.boardItems.filter(function (i) {
                            return i.Id == self.attributes.dbId;
                        })[0];
                        var position = this.position();
                        cll.PositionX = position.x;
                        cll.PositionY = position.y;
                        signalRProxy.invoke("ChangePosition", cll, function (errorMessage, modelErrors) {
                            $scope.error = true;
                            $scope.errorMessage = errorMessage;
                            $scope.modelErrors = modelErrors;
                        });
                    }
                });
            }
            switch(cell.attributes.type) {
                case "uml.Class":
                    clon.attributes.classTypeId = 1;
                    break;
                case "uml.Abstract":
                    clon.attributes.classTypeId = 2;
                    break;
                case "uml.Interface":
                    clon.attributes.classTypeId = 3;
                    break;
            }
            $scope.boardGraph.addCell(clon);
        }

        if (graph.cid == $scope.boardGraph.cid && !cell.isLink()) {
            var cll = $scope.boardItems.filter(function (i) {
                return i.Id == cell.attributes.dbId;
            })[0];
            if (!cll) {
                $scope.selectedItem = {
                    Name: cell.attributes.name,
                    Attributes: [],
                    Methods: [],
                    BoardId: board.Id,
                    jointId: cell.id,
                    ClassTypeId: cell.attributes.classTypeId
                };
            }else {
                $scope.selectedItem = cll;                
            }            
            $scope.$apply();
        }
    });    

    $scope.addAttribute = function (attribute) {
        if (attribute) {
            $scope.selectedItem.Attributes.push({ Name: attribute, ClassId: $scope.selectedItem.Id });
            $scope.selectedItem.newAttribute = "";
        }
    };
    
    $scope.deleteAttribute = function (index) {        
        $scope.selectedItem.Attributes.splice(index, 1);        
    };

    $scope.addMethod = function (method) {
        if (method) {
            $scope.selectedItem.Methods.push({ Name: method, ClassId: $scope.selectedItem.Id });
            $scope.selectedItem.newMethod = "";
        }
    };

    $scope.deleteMethod = function(index) {
        $scope.selectedItem.Methods.splice(index, 1);        
    };

    $scope.saveItem = function () {        
        var cell = $scope.boardGraph.getCell($scope.selectedItem.jointId);
        $scope.selectedItem.BoardId = board.Id;
        $scope.selectedItem.PositionX = cell.attributes.position.x;
        $scope.selectedItem.PositionY = cell.attributes.position.y;
        signalRProxy.invoke("SaveClass", $scope.selectedItem,
                function (errorMessage, modelErrors) {
                    $scope.error = true;
                    $scope.errorMessage = errorMessage;
                    $scope.modelErrors = modelErrors;
                });        
    };

    $scope.deleteItem = function() {
        var cell = $scope.boardGraph.getCell($scope.selectedItem.jointId);
        if (cell.attributes.dbId) {
            $scope.boardGraph.removeLinks(cell);
            signalRProxy.invoke("DeleteClass", $scope.selectedItem, function(errorMessage, modelErrors) {
                $scope.error = true;
                $scope.errorMessage = errorMessage;
                $scope.modelErrors = modelErrors;
            });
        } else {
            cell.remove();
        }
        $scope.selectedItem = null;
    };

    $scope.$on("linkDelete", function(data, cell, graph) {
        var linkToDelete = $scope.boardLinks.filter(function(i) {
            return i.Id == cell.attributes.dbId;
        })[0];
        if(linkToDelete) {
            signalRProxy.invoke("DeleteLink", linkToDelete, function(errorMessage, modelErrors) {
                $scope.error = true;
                $scope.errorMessage = errorMessage;
                $scope.modelErrors = modelErrors;
            });
        }
    });

    $scope.closeSelected = function() {
        $scope.selectedItem = null;
    };

    function saveLink(l) {
        if (l.attributes.source.id && l.attributes.target.id) {
            var srcId = $scope.boardGraph.getCell(l.attributes.source.id).attributes.dbId;
            var tgtId = $scope.boardGraph.getCell(l.attributes.target.id).attributes.dbId;
            if (!(srcId && tgtId)) return;
            var link = $scope.boardLinks.filter(function(i) {
                return i.Id == l.attributes.dbId;
            })[0];
            if (!link) {
                $scope.selectedLink = {
                    BoardId: board.Id                    
                };                
            } else {
                $scope.selectedLink = link;
            }
            $scope.selectedLink.SourceClassId = srcId;
            $scope.selectedLink.TargetClassId = tgtId;
            $scope.selectedLink.jointId = l.id;
            switch (l.attributes.type) {
                case "uml.Generalization":
                    $scope.selectedLink.RelationshipTypeId = 2;
                    break;
                case "uml.Implementation":
                    $scope.selectedLink.RelationshipTypeId = 3;
                    break;
                case "uml.Aggregation":
                    $scope.selectedLink.RelationshipTypeId = 4;
                    break;
                case "uml.Composition":
                    $scope.selectedLink.RelationshipTypeId = 5;
                    break;
            }
            signalRProxy.invoke("SaveLink", $scope.selectedLink, function (errorMessage, modelErrors) {
                $scope.error = true;
                $scope.errorMessage = errorMessage;
                $scope.modelErrors = modelErrors;
            });
        }
    }            

    $scope.logOut = function () {
        authService.logOut();
        $location.path("/login");
    };
    
    $scope.closeAlert = function () {
        $scope.error = false;
    };

}]);