﻿describe("Board", function() {

    beforeEach(module("app"));

    var ctrl, scope;
    // inject the $controller and $rootScope services
    // in the beforeEach block
    beforeEach(inject(function ($controller, $rootScope) {
        // Create a new scope that's a child of the $rootScope
        scope = $rootScope.$new();
        // Create the controller
        ctrl = $controller('boardController', {
            $scope: scope
        });
    }));

    it("has been initialised", function () {
        expect(scope).toBeDefined();
    });

    it("has 3 items in the elements menu", function() {
        var elements = scope.elementsGraph.getElements();
        expect(elements.length).toBe(3);
    });

    it("has 4 items in the relationships menu", function() {
        var elements = scope.linksGraph.getLinks();
        expect(elements.length).toBe(4);
    });

    describe("when an attribute is added", function() {

        it("the attributes collection has one more item", function() {
            scope.selectedItem = {
                Attributes: []
            };
            var originalLength = scope.selectedItem.Attributes.length;
            scope.addAttribute("newAttribute");
            expect(scope.Attributes.length == originalLength + 1);
        });
    });
    
    describe("when an attribute is deleted", function () {

        it("the attributes collection has one item less", function () {
            scope.selectedItem = {
                Attributes: ["attribute"]
            };
            var originalLength = scope.selectedItem.Attributes.length;
            scope.deleteAttribute(0);
            expect(scope.Attributes.length == originalLength - 1);
        });
    });

    describe("when a method is added", function () {

        it("the methods collection has one more item", function () {
            scope.selectedItem = {
                Methods: []
            };
            var originalLength = scope.selectedItem.Methods.length;
            scope.addAttribute("newMethod");
            expect(scope.Methods.length == originalLength + 1);
        });
    });
    
    describe("when a method is deleted", function () {

        it("the methods collection has one item less", function () {
            scope.selectedItem = {
                Methods: ["method"]
            };
            var originalLength = scope.selectedItem.Methods.length;
            scope.deleteMethod(0);
            expect(scope.Methods.length == originalLength - 1);
        });
    });
});