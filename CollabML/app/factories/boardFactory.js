﻿app.factory("boardFactory", ["$q", "$resource", function($q, $resource) {
    var resource = $resource("api/board/:id", { id: '@id' });

    var get = function(id) {
        var deferred = $q.defer();
        resource.get({ id: id }, function(board) {
            deferred.resolve(board);
        }, function(response) {
            deferred.reject(response);
        });

        return deferred.promise;
    };

    return {
        get: get
    };

}]);