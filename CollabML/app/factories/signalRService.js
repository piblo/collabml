﻿app.factory('signalRService', ['$rootScope', function ($rootScope) {    
    function signalRHubProxy(serverUrl, hubName, queryString) {            

        var proxy = null;
        var connection = null;

        //Getting the connection object
        connection = $.hubConnection(serverUrl);        

        //Creating proxy   
        proxy = connection.createHubProxy(hubName);

        if (queryString) {
            connection.qs = queryString;
        }

        var connect = function() {
            //Starting connection
            connection.start();
        };

        var on = function(event, callback) {
            //Publishing an event when server pushes an update
            proxy.on(event, function(data) {
                $rootScope.$apply(function() {
                    if (callback) {
                        callback(data);
                    }
                });
            });
        };

        var off = function(event, callback) {
            proxy.off(event, function(data) {
                $rootScope.$apply(function() {
                    if (callback) {
                        callback(data);
                    }
                });
            });
        };

        var invoke = function (action, data, fail) {
            proxy.invoke(action, data).fail(function (e) { $rootScope.$apply(fail(e.message, e.data)); });
        };

        return {
            connect: connect,
            invoke: invoke,
            on: on,
            off: off            
        };

    }

    return signalRHubProxy;

}]);