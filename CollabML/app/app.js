﻿
var app = angular.module("app", ["ngRoute", "ui.bootstrap", "ngResource", "LocalStorageModule", "ngAnimate"])
    .config(["$routeProvider", function($routeProvider) {
        $routeProvider
            .when('/board',
                {
                    controller: 'boardController',
                    templateUrl: '/app/partials/board.html'
                })
            .when('/login',
                {
                    controller: 'loginController',
                    templateUrl: '/app/partials/login.html'
                })
            .when('/signup',
                {
                    controller: 'signupController',
                    templateUrl: '/app/partials/signup.html'
                })
            .otherwise({ redirectTo: '/login' });
    }])
    .run(['authService', '$location', function(authService, $location) {
        authService.fillAuthData();
        if (authService.authentication && authService.authentication.isAuth) {
            $location.path("/board");
        }
    }])
    .config(function($httpProvider) {
        $httpProvider.interceptors.push('authInterceptorService');
    })
    .constant("APIURL", "/");