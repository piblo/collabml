﻿
app.directive('jointDiagram', ['$compile', function() {

    var jointDiagramDirective = {
        restrict: 'E',
        scope: {
            width: '=width',
            height: '=height',
            group: '=group',
            graph: '=graph',
            interactive: '=interactive',
            items: '=items'
        },
        
        //scope: true,
        link: function ($scope, element, attrs) {
            var graph = $scope.graph;
            var paper = new joint.dia.Paper({
                el: $(element),
                width: $scope.width,
                height: $scope.height,
                gridSize: 1,
                model: graph,
                interactive: $scope.interactive
            });
            
            var elements = graph.attributes.cells.models;
            graph.resetCells(elements);

            paper.on("cell:pointerclick", function(cellView, evt, x, y) {
                var cell = cellView.model;
                if (!cell.isLink()) {
                    var allViews = document.querySelectorAll("g");
                    for (var i = 0; i < allViews.length; i++) {
                        var v = allViews[i];
                        v.className.animVal = v.className.animVal.replace("highlighted", "");
                        v.className.baseVal = v.className.baseVal.replace("highlighted", "");
                    }
                    var view = document.querySelector("g[model-id='" + cell.id + "']");
                    view.className.animVal += " highlighted";
                    view.className.baseVal += " highlighted";
                }
                $scope.$emit('cellPointerClick', cell, graph);
            });

            graph.on('remove', function (cell) {
                if (cell.isLink() && cell.attributes.dbId) {                    
                    $scope.$emit("linkDelete", cell, graph);                    
                }
            });
        }
    };
            
    return jointDiagramDirective;
}]);