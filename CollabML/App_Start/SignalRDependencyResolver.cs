﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.SignalR;
using Ninject;

namespace CollabML.Web.App_Start
{
    public class SignalRDependencyResolver : DefaultDependencyResolver
    {
        private readonly IKernel _kernel;

        public SignalRDependencyResolver(IKernel kernel)
        {
            if (kernel == null)
            {
                throw new ArgumentNullException("kernel");
            }

            _kernel = kernel;
        }

        public override object GetService(Type serviceType)
        {
            var service = _kernel.TryGet(serviceType) ?? base.GetService(serviceType);
            return service;
        }

        public override IEnumerable<object> GetServices(Type serviceType)
        {
            var services = _kernel.GetAll(serviceType).Concat(base.GetServices(serviceType));
            return services;
        }
    }
}