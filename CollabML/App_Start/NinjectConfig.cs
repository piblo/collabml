﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CollabML.Data.Contracts;
using CollabML.Data.Repositories;
using CollabML.Identity;
using CollabML.Web.Services;
using Ninject;

namespace CollabML.Web.App_Start
{
    public class NinjectConfig
    {
        public static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();

            kernel.Bind<IBoardRepository>().To<BoardRepository>();
            kernel.Bind<IClassRepository>().To<ClassRepository>();
            kernel.Bind<IIdentityRepository>().To<IdentityRepository>();
            kernel.Bind<IRelationshipRepository>().To<RelationshipRepository>();
            //kernel.Bind<IRelationshipTypeRepository>().To<RelationshipTypeRepository>();
            kernel.Bind<IUserRepository>().To<UserRepository>();
            kernel.Bind<IAttributeRepository>().To<AttributeRepository>();
            kernel.Bind<IMethodRepository>().To<MethodRepository>();

            kernel.Bind<UserService>().To<UserService>();

            return kernel;
        }
    }
}