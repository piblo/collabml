﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CollabML.Web.Models
{
    public class RelationshipModel
    {
        public int Id { get; set; }
        [Range(1, int.MaxValue)]
        public int SourceClassId { get; set; }
        [Range(1, int.MaxValue)]
        public int TargetClassId { get; set; }
        [Range(1, int.MaxValue)]
        public int BoardId { get; set; }
        [Range(1, 5)]
        public int RelationshipTypeId { get; set; }
    }
}