﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CollabML.Web.Models
{
    public class ModelValidator
    {
        public List<ValidationResult> ValidationResults = new List<ValidationResult>();
        public bool ValidateModel(object model)
        {
            var context = new ValidationContext(model, null, null);
            var isvalid = Validator.TryValidateObject(model, context, ValidationResults, true);

            return isvalid;
        }
    }
}