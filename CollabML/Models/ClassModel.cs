﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CollabML.Data.Model;

namespace CollabML.Web.Models
{
    public class ClassModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [Range(1, 3)]
        public int ClassTypeId { get; set; }
        public ClassType ClassType { get; set; }
        public int PositionX { get; set; }
        public int PositionY { get; set; }

        public virtual List<Attribute> Attributes { get; set; }
        public virtual List<Method> Methods { get; set; }

        [Range(1, int.MaxValue)]
        public int BoardId { get; set; }
    }
}