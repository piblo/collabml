﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using CollabML.Data.Contracts;
using CollabML.Data.Model;
using CollabML.Web.Services;

namespace CollabML.Web.Controllers
{    
    public class BoardController : ApiController
    {
        private IBoardRepository boardRepository;
        private UserService userService;

        public BoardController(IBoardRepository boardRepository, UserService userService)
        {
            this.boardRepository = boardRepository;
            this.userService = userService;
        }

        [Authorize]
        public Board Get ()
        {
            var user = userService.GetUser(User.Identity.Name);
            var board = boardRepository.GetUserBoards(user.Id).FirstOrDefault();
            return board;
        }
    }
}