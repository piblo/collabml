﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using CollabML.Data.Contracts;
using CollabML.Data.Model;
using CollabML.Web.Models;

namespace CollabML.Web.Controllers
{
    public class AccountController : ApiController
    {
        private IIdentityRepository _identityRepository;
        private IUserRepository _userRepository;        

        public AccountController(IIdentityRepository identityRepository, IUserRepository userRepository)
        {
            _identityRepository = identityRepository;
            _userRepository = userRepository;
        }

        [AllowAnonymous]
        [HttpPost]
        public IHttpActionResult Register(UserModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = new User
            {
                UserName = model.UserName,
                Password = model.Password
            };
            try
            {
                var result = _identityRepository.RegisterUser(user);
                var errors = GetErrorResult(result);
                if (errors != null)
                    return errors;

                user = _identityRepository.GetUser(model.UserName);
                var appUser = new User
                {
                    UserId = user.UserId,
                    Name = model.Name,
                    Surname = model.Surname,
                    UserName = model.UserName
                };                

                _userRepository.AddUser(appUser);
                return Ok();
            }
            catch (Exception e)
            {
                ModelState.AddModelError(e.GetType().ToString(), e.Message);                
                return BadRequest(ModelState);
            }
        }

        private IHttpActionResult GetErrorResult(IdentityResultModel result)
        {
            if (result == null)
                return InternalServerError();

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }
            return null;
        }
    }
}
