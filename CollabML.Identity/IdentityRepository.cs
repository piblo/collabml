﻿using System;
using CollabML.Data.Contracts;
using CollabML.Data.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CollabML.Identity
{
    public class IdentityRepository : IIdentityRepository 
    {
        private IdentityContext _db = new IdentityContext();
        private UserManager<IdentityUser> _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>());

        public IdentityResultModel RegisterUser(User user)
        {
            var identityUser = new IdentityUser(user.UserName);
            identityUser.Email = user.UserName;
            var identityResult = _userManager.Create(identityUser, user.Password);
            return MapToIdentityResultModel(identityResult);
        }

        public User GetUser(string userName)
        {
            var identityUser = _userManager.FindByName(userName);
            return MapToUser(identityUser);
        }

        public User GetUser(string userName, string password)
        {
            var identityUser = _userManager.Find(userName, password);
            if (identityUser == null)
                return null;

            return MapToUser(identityUser);
        }       

        public void Dispose()
        {
            _db.Dispose();
        }

        #region Utility Methods

        private User MapToUser(IdentityUser iu)
        {
            var user = new User
            {
                UserId = Guid.Parse(iu.Id),
                UserName = iu.UserName,
                Password = iu.PasswordHash
            };
            return user;
        }

        private IdentityResultModel MapToIdentityResultModel(IdentityResult ir)
        {
            var model = new IdentityResultModel
            {
                Succeeded = ir.Succeeded,
                Errors = ir.Errors
            };
            return model;
        }

        #endregion
    }
}
