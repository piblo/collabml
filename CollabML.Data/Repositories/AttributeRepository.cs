﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollabML.Data.Contracts;

namespace CollabML.Data.Repositories
{
    public class AttributeRepository : IAttributeRepository
    {
        private CollabMlContext _db = new CollabMlContext();

        public Model.Attribute GetAttribute(int attributeId)
        {
            return _db.Attributes.Find(attributeId);
        }

        IQueryable<Model.Attribute> IAttributeRepository.GetClassAttributes(int classId)
        {
            return _db.Attributes.Where(x => x.ClassId == classId);
        }

        public Model.Attribute AddAttribute(Model.Attribute attribute)
        {
            var attr = _db.Attributes.Add(attribute);
            _db.SaveChanges();
            return attr;
        }

        public bool SaveChanges(Model.Attribute attribute)
        {
            var attr = _db.Attributes.Attach(attribute);
            _db.Entry(attr).State = EntityState.Modified;
            return _db.SaveChanges() > 0;
        }

        Model.Attribute IAttributeRepository.DeleteAttribute(int attributeId)
        {
            var attr = _db.Attributes.Find(attributeId);
            attr = _db.Attributes.Remove(attr);
            _db.SaveChanges();
            return attr;
        }        
    }
}
