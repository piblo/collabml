﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollabML.Data.Contracts;
using CollabML.Data.Model;

namespace CollabML.Data.Repositories    
{
    public class BoardRepository : IBoardRepository
    {
        private CollabMlContext _db = new CollabMlContext();

        public IEnumerable<Board> GetUserBoards(int userId)
        {
            var user = _db.Users.Include("Boards").SingleOrDefault(x => x.Id == userId);            
            return user.Boards;
        }
    }
}
