﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollabML.Data.Contracts;
using CollabML.Data.Model;

namespace CollabML.Data.Repositories
{
    public class ClassRepository : IClassRepository
    {
        private CollabMlContext _db = new CollabMlContext();

        public Class GetClass(int classId)
        {
            return _db.Classes.Find(classId);
        }

        public IQueryable<Class> GetBoardClasses(int boardId)
        {
            return _db.Classes.Where(x => x.BoardId == boardId);
        }

        public Class AddClass(Class cls)
        {
            var cl = _db.Classes.Add(cls);
            _db.SaveChanges();
            return cl;
        }

        public bool SaveChanges(Class cls)
        {
            var attrs = cls.Attributes.ToList();
            var methods = cls.Methods.ToList();
            var cl = _db.Classes.Attach(cls);

            var attrToDelete = _db.Attributes.Where(x => x.ClassId == cls.Id).AsEnumerable().Where(x => attrs.All(a => a.Id != x.Id));
            _db.Attributes.RemoveRange(attrToDelete);       

            var mthToDelete = _db.Methods.Where(x => x.ClassId == cls.Id).AsEnumerable().Where(x => methods.All(m => m.Id != x.Id));            
            _db.Methods.RemoveRange(mthToDelete);

            _db.Entry(cls).State = EntityState.Modified;
            return _db.SaveChanges() > 0;
        }

        public async Task<bool> UpdatePosition(Class cls)
        {
            var x = cls.PositionX;
            var y = cls.PositionY;
            _db.Classes.Attach(cls);
            cls.PositionX = x;
            cls.PositionY = y;
            _db.Entry(cls).State = EntityState.Modified;            
            return await _db.SaveChangesAsync() > 0;
        }

        public Class DeleteClass(int classId)
        {
            var cl = _db.Classes.Find(classId);            
            cl = _db.Classes.Remove(cl);
            _db.SaveChanges();
            return cl;
        }
    }
}
