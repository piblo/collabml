﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollabML.Data.Contracts;
using CollabML.Data.Model;

namespace CollabML.Data.Repositories
{
    public class RelationshipRepository : IRelationshipRepository
    {
        private CollabMlContext _db = new CollabMlContext();
        public Relationship GetRelationship(int relationshipId)
        {
            return _db.Relationships.Find(relationshipId);
        }

        public IQueryable<Relationship> GetBoardRelationships(int boardId)
        {
            return _db.Relationships.Where(x => x.BoardId == boardId);
        }

        public Relationship AddRelationship(Relationship relationship)
        {
            var rl = _db.Relationships.Add(relationship);
            _db.SaveChanges();
            return rl;
        }

        public Relationship DeleteRelationship(Relationship relationship)
        {
            var rl = _db.Relationships.Attach(relationship);
            rl = _db.Relationships.Remove(rl);
            _db.SaveChanges();
            return rl;
        }

        public bool SaveChanges(Relationship relationship)
        {
            var rl = _db.Relationships.Attach(relationship);
            _db.Entry(rl).State = EntityState.Modified;
            return _db.SaveChanges() > 0;
        }
    }
}
