﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollabML.Data.Contracts;
using CollabML.Data.Model;

namespace CollabML.Data.Repositories
{
    public class MethodRepository : IMethodRepository
    {
        private CollabMlContext _db = new CollabMlContext();

        public Method GetMethod(int methodId)
        {
            return _db.Methods.Find(methodId);
        }

        public IQueryable<Method> GetClassMethods(int classId)
        {
            return _db.Methods.Where(x => x.ClassId == classId);
        }

        public Method AddMethod(Method method)
        {
            var mth = _db.Methods.Add(method);
            _db.SaveChanges();
            return mth;
        }

        public Method DeleteMethod(int methodId)
        {
            var mth = _db.Methods.Find(methodId);
            mth = _db.Methods.Remove(mth);
            _db.SaveChanges();
            return mth;
        }

        public bool SaveChanges(Method method)
        {
            var mth = _db.Methods.Attach(method);
            _db.Entry(mth).State = EntityState.Modified;
            return _db.SaveChanges() > 0;
        }
    }
}
