﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollabML.Data.Contracts;
using CollabML.Data.Model;

namespace CollabML.Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        private CollabMlContext _db = new CollabMlContext();
        public User GetUser(int id)
        {
            return _db.Users.Find(id);
        }

        public User GetUser(Guid userId)
        {
            return _db.Users.Single(x => x.UserId == userId);
        }

        public User AddUser(User user)
        {
            user = _db.Users.Add(user);
            //HACK: Only supports one board for proof of concept purposes
            var board = _db.Boards.FirstOrDefault();
            user.Boards = new List<Board> { board };
            _db.SaveChanges();
            return user;
        }

        public bool SaveChanges(User user)
        {
            user = _db.Users.Attach(user);
            _db.Entry(user).State = EntityState.Modified;
            return _db.SaveChanges() > 0;
        }        
    }
}
