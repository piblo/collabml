// <auto-generated />
namespace CollabML.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ModelRevision : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ModelRevision));
        
        string IMigrationMetadata.Id
        {
            get { return "201504281821486_ModelRevision"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
