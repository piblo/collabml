namespace CollabML.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Boards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Classes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ClassTypeId = c.Int(nullable: false),
                        BoardId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClassTypes", t => t.ClassTypeId, cascadeDelete: true)
                .Index(t => t.ClassTypeId);
            
            CreateTable(
                "dbo.ClassTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Relationships",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SourceClassId = c.Int(nullable: false),
                        TargetClassId = c.Int(nullable: false),
                        CardinalitySrc = c.String(),
                        CardinalityTgt = c.String(),
                        BoardId = c.Int(nullable: false),
                        RelationshipTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RelationshipTypes", t => t.RelationshipTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Classes", t => t.SourceClassId)
                .ForeignKey("dbo.Classes", t => t.TargetClassId)
                .Index(t => t.SourceClassId)
                .Index(t => t.TargetClassId)
                .Index(t => t.RelationshipTypeId);
            
            CreateTable(
                "dbo.RelationshipTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        UserName = c.String(nullable: false),
                        Name = c.String(),
                        Surname = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Relationships", "TargetClassId", "dbo.Classes");
            DropForeignKey("dbo.Relationships", "SourceClassId", "dbo.Classes");
            DropForeignKey("dbo.Relationships", "RelationshipTypeId", "dbo.RelationshipTypes");
            DropForeignKey("dbo.Classes", "ClassTypeId", "dbo.ClassTypes");
            DropIndex("dbo.Relationships", new[] { "RelationshipTypeId" });
            DropIndex("dbo.Relationships", new[] { "TargetClassId" });
            DropIndex("dbo.Relationships", new[] { "SourceClassId" });
            DropIndex("dbo.Classes", new[] { "ClassTypeId" });
            DropTable("dbo.Users");
            DropTable("dbo.RelationshipTypes");
            DropTable("dbo.Relationships");
            DropTable("dbo.ClassTypes");
            DropTable("dbo.Classes");
            DropTable("dbo.Boards");
        }
    }
}
