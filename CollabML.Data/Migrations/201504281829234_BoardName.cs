namespace CollabML.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BoardName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Boards", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Boards", "Name");
        }
    }
}
