namespace CollabML.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelRevision : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attributes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ClassId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Classes", t => t.ClassId, cascadeDelete: true)
                .Index(t => t.ClassId);
            
            CreateTable(
                "dbo.Methods",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ClassId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Classes", t => t.ClassId, cascadeDelete: true)
                .Index(t => t.ClassId);
            
            CreateTable(
                "dbo.UserBoards",
                c => new
                    {
                        User_Id = c.Int(nullable: false),
                        Board_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Board_Id })
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.Boards", t => t.Board_Id, cascadeDelete: true)
                .Index(t => t.User_Id)
                .Index(t => t.Board_Id);
            
            AddColumn("dbo.Classes", "PositionX", c => c.Int(nullable: false));
            AddColumn("dbo.Classes", "PositionY", c => c.Int(nullable: false));
            CreateIndex("dbo.Classes", "BoardId");
            CreateIndex("dbo.Relationships", "BoardId");
            AddForeignKey("dbo.Classes", "BoardId", "dbo.Boards", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Relationships", "BoardId", "dbo.Boards", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserBoards", "Board_Id", "dbo.Boards");
            DropForeignKey("dbo.UserBoards", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Relationships", "BoardId", "dbo.Boards");
            DropForeignKey("dbo.Classes", "BoardId", "dbo.Boards");
            DropForeignKey("dbo.Methods", "ClassId", "dbo.Classes");
            DropForeignKey("dbo.Attributes", "ClassId", "dbo.Classes");
            DropIndex("dbo.UserBoards", new[] { "Board_Id" });
            DropIndex("dbo.UserBoards", new[] { "User_Id" });
            DropIndex("dbo.Relationships", new[] { "BoardId" });
            DropIndex("dbo.Methods", new[] { "ClassId" });
            DropIndex("dbo.Classes", new[] { "BoardId" });
            DropIndex("dbo.Attributes", new[] { "ClassId" });
            DropColumn("dbo.Classes", "PositionY");
            DropColumn("dbo.Classes", "PositionX");
            DropTable("dbo.UserBoards");
            DropTable("dbo.Methods");
            DropTable("dbo.Attributes");
        }
    }
}
