﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollabML.Data.Model;

namespace CollabML.Data
{
    class CollabMlContextInitializer : DropCreateDatabaseIfModelChanges<CollabMlContext>
    {
        protected override void Seed(CollabMlContext context)
        {
            #region Lookup Data

            //context.Accessibilities.Add(new Accessibility("public"));
            //context.Accessibilities.Add(new Accessibility("private"));
            //context.Accessibilities.Add(new Accessibility("protected"));

            //context.Cardinalities.Add(new Cardinality("0"));
            //context.Cardinalities.Add(new Cardinality("1"));
            //context.Cardinalities.Add(new Cardinality("*"));

            //context.DataTypes.Add(new DataType("int"));
            //context.DataTypes.Add(new DataType("bool"));
            //context.DataTypes.Add(new DataType("string"));
            //context.DataTypes.Add(new DataType("double"));
            //context.DataTypes.Add(new DataType("char"));
            //context.DataTypes.Add(new DataType("date"));

            context.ClassTypes.Add(new ClassType("class"));
            context.ClassTypes.Add(new ClassType("abstract"));
            context.ClassTypes.Add(new ClassType("interface"));            

            context.RelationshipTypes.Add(new RelationshipType("association"));
            context.RelationshipTypes.Add(new RelationshipType("generalization"));
            context.RelationshipTypes.Add(new RelationshipType("implementation"));
            context.RelationshipTypes.Add(new RelationshipType("aggregation"));
            context.RelationshipTypes.Add(new RelationshipType("composition"));

            context.SaveChanges();

            #endregion Lookup Data

            
        }
    }
}
