﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollabML.Data.Model;

namespace CollabML.Data
{    
    public class CollabMlContext : DbContext
    {
        public DbSet<CollabML.Data.Model.Attribute> Attributes { get; set; }
        public DbSet<Board> Boards { get; set; }
        public DbSet<Class> Classes { get; set; }        
        public DbSet<ClassType> ClassTypes { get; set; }
        public DbSet<Method> Methods { get; set; }
        public DbSet<Relationship> Relationships { get; set; }
        public DbSet<RelationshipType> RelationshipTypes { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Class>().HasMany(x => x.Relationships).WillCascadeOnDelete(false);
        }
    }
}
