﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollabML.Data.Model;

namespace CollabML.Data.Contracts
{
    public interface IClassRepository
    {
        Class GetClass(int classId);
        IQueryable<Class> GetBoardClasses(int boardId);
        Class AddClass(Class cls);
        bool SaveChanges(Class cls);
        Task<bool> UpdatePosition(Class cls);
        Class DeleteClass(int classId);
    }
}
