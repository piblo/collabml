﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollabML.Data.Model;

namespace CollabML.Data.Contracts
{
    public interface IBoardRepository
    {
        IEnumerable<Board> GetUserBoards(int userId);
    }
}
