﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollabML.Data.Model;

namespace CollabML.Data.Contracts
{
    public interface IRelationshipRepository
    {
        Relationship GetRelationship(int relationshipId);
        IQueryable<Relationship> GetBoardRelationships(int boardId);
        Relationship AddRelationship(Relationship relationship);
        Relationship DeleteRelationship(Relationship relationship);
        bool SaveChanges(Relationship relationship);
    }
}
