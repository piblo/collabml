﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollabML.Data.Model;

namespace CollabML.Data.Contracts
{
    public interface IMethodRepository
    {
        Method GetMethod(int methodId);
        IQueryable<Method> GetClassMethods(int classId);
        Method AddMethod(Method method);
        Method DeleteMethod(int methodId);
        bool SaveChanges(Method method);
    }
}
