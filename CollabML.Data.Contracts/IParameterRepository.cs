﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollabML.Data.Model;

namespace CollabML.Data.Contracts
{
    public interface IParameterRepository
    {
        Parameter GetParameter(int parameterId);
        IQueryable<Parameter> GetMethodParameters(int methodId);
        Parameter AddParameter(Parameter parameter);
        Parameter DeleteParameter(Parameter parameter);
        bool SaveChanges(Parameter parameter);
    }
}
