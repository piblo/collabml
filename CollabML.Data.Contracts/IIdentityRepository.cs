﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollabML.Data.Model;

namespace CollabML.Data.Contracts
{
    public interface IIdentityRepository : IDisposable
    {
        IdentityResultModel RegisterUser(User user);
        User GetUser(string email);
        User GetUser(string userName, string password);        
    }
}
