﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollabML.Data.Model;

namespace CollabML.Data.Contracts
{
    public interface IUserRepository
    {
        User GetUser(int id);
        User GetUser(Guid userId);
        User AddUser(User user);
        bool SaveChanges(User user);        
    }
}
