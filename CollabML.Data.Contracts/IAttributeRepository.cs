﻿using System.Linq;
using CollabML.Data.Model;

namespace CollabML.Data.Contracts
{
    public interface IAttributeRepository
    {
        Attribute GetAttribute(int attributeId);
        IQueryable<Attribute> GetClassAttributes(int classId);
        Attribute AddAttribute(Attribute attribute);
        bool SaveChanges(Attribute attribute);
        Attribute DeleteAttribute(int attributeId);
    }
}
